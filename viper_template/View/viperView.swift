//
//  viperView.swift
//  viper_template
//
//  Created by kira on 8/27/19.
//  Copyright (c) 2019 Dmitry. All rights reserved.
//

import UIKit

protocol viperView: class {

    func displaySomething(viewModel: viper.Entity.ViewModel)
}
